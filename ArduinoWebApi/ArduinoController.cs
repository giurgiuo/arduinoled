﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ArduinoWebApi
{
    public class ArduinoController : ApiController
    {
        [Route("api/led/{state}")]
        public async Task<IHttpActionResult> SetLedState(bool state)
        {

            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get, "http://192.168.0.20/led=0");
                if (state)
                {
                    request = new HttpRequestMessage(HttpMethod.Get, "http://192.168.0.20/led=1");
                }
                
                var result = client.SendAsync(request).Result;
                if (result.IsSuccessStatusCode)
                {
                    return Ok("success");
                }

                return BadRequest("failed");
            }
        }
    }
}