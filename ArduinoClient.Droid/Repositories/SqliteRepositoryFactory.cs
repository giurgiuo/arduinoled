using ArduinoClient.Repository;
using SQLite.Net;

namespace ArduinoClient.Droid.Repositories
{
    public class SqliteRepositoryFactory : IRepositoryFactory
    {
        private readonly SQLiteConnection _connection;

        public SqliteRepositoryFactory(SQLiteConnection connection)
        {
            _connection = connection;
        }

        public IRepository<T> Create<T>() where T : class, new()
        {
            return new SqliteRepository<T>(_connection);
        }
    }
}