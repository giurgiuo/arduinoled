﻿using System.Collections.Generic;
using ArduinoClient.Repository;
using SQLite.Net;

namespace ArduinoClient.Droid.Repositories
{
    class SqliteRepository<T> : IRepository<T> where T : class, new() 
    {
        private readonly SQLiteConnection _connection;

        public SqliteRepository(SQLiteConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<T> GetData()
        {
            var data = _connection.Table<T>();

            return data;
        }

        public void Insert(T entry)
        {
            _connection.Insert(entry);
        }

        public void Update(T entry)
        {
            _connection.Update(entry);
        }

        public void Delete(T entry)
        {
            _connection.Delete(entry);
        }
    }
}
