using System.IO;
using Android.Content;
using Android.OS;
using ArduinoClient.Droid.Repositories;
using ArduinoClient.Models;
using ArduinoClient.Repository;
using ArduinoClient.Services;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.File;
using MvvmCross.Plugins.File.Droid;
using MvvmCross.Plugins.Sqlite;
using SQLite.Net;

namespace ArduinoClient.Droid
{
    public static class DependenciesSetup

    {
        const string DatabaseDirectory = "Led";
        const string DatabaseFileName = "led.sqlite";
        private const string LogFileName = "log.txt";

        public static void Setup(Context context)
        {
            var sdcardPath = Environment.ExternalStorageDirectory.Path;
            var databasePath = Path.Combine(sdcardPath, DatabaseDirectory);
            if (!Directory.Exists(databasePath))
            {
                Directory.CreateDirectory(databasePath);
            }
            var database = Path.Combine(databasePath, DatabaseFileName);

            if (!File.Exists(database))
            {
                File.Create(database);
            }

            var logPath = Path.Combine(databasePath, LogFileName);
            if (!File.Exists(logPath))
            {
                File.Create(logPath);
            }

            Mvx.RegisterSingleton(() =>
            {
                var factory = Mvx.Resolve<IMvxSqliteConnectionFactory>();
                var connection = factory.GetConnection(database);
                connection.CreateTable<Log>();
                connection.CreateTable<Audit>();

                return connection;
            });

            Mvx.RegisterSingleton<IRepositoryFactory>(() =>
            {
                var connection = Mvx.Resolve<SQLiteConnection>();
                var sqliteRepositoryFactory = new SqliteRepositoryFactory(connection);
                return sqliteRepositoryFactory;
            });

            Mvx.RegisterSingleton<IEventService>(() =>
            {
                var factory = Mvx.Resolve<IRepositoryFactory>();

                var eventService = new EventService(factory);
                return eventService;
            });

            Mvx.RegisterSingleton<IMvxFileStore>(() =>
            {
                var file = new MvxAndroidFileStore();
                return file;
            });

            Mvx.RegisterSingleton<ILogger>(() =>
            {
                var logger = new Logger(logPath, Mvx.Resolve<IMvxFileStore>());
                return logger;
            });

            Mvx.RegisterSingleton<IMvxJsonConverter>(Mvx.Resolve<IMvxJsonConverter>);
        }
    }
}