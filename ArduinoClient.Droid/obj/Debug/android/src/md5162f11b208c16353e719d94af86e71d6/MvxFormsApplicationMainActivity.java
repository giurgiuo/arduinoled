package md5162f11b208c16353e719d94af86e71d6;


public class MvxFormsApplicationMainActivity
	extends md5530bd51e982e6e7b340b73e88efe666e.FormsApplicationActivity
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("ArduinoClient.Droid.MvxFormsApplicationMainActivity, ArduinoClient.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MvxFormsApplicationMainActivity.class, __md_methods);
	}


	public MvxFormsApplicationMainActivity () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MvxFormsApplicationMainActivity.class)
			mono.android.TypeManager.Activate ("ArduinoClient.Droid.MvxFormsApplicationMainActivity, ArduinoClient.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
