using MvvmCross.Platform.Plugins;

namespace ArduinoClient.Droid.Bootstrap
{
    public class FilePluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader>
    {
    }
}