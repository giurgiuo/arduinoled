using MvvmCross.Platform.Plugins;

namespace ArduinoClient.Droid.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Sqlite.PluginLoader>
    { }
}
