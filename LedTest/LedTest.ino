#include <SPI.h>
#include <Ethernet.h>

#define _baudRate 9600
#define _ledPin 13

String _httpReq;
boolean _ledStatus = false;
// Random MAC address for Ethernet shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192, 168, 0, 20); // IP address, may need to change depending on network
EthernetServer server(80);  // create a server at port 80
// Add gateway and subnet for Enthernet.begin() if the page cannot be reached
//byte gateway[] = { 192, 168, 0, 1 };
//byte subnet[] = { 255, 255, 0, 0 };

void setup()
{
	// Enthernet connection
	/*while (!Serial)
	{
		Serial.println("Wait for connection");
		delay(1000);
	}*/

	Ethernet.begin(mac, ip);  // initialize Ethernet device
	server.begin();           // start to listen for clients

	Serial.print("Server is at ");
	Serial.println(Ethernet.localIP());

	// serial communication
	Serial.begin(_baudRate);
	pinMode(_ledPin, OUTPUT);
}

void loop()
{
	EthernetClient client = server.available();  // try to get client

	// got client?
	if (client)
	{  		
		Serial.println("new client");

		boolean currentLineIsBlank = true;
		while (client.connected()) 
		{
			if (client.available()) 
			{   
				// client data available to read
				// read 1 byte (character) from client
				char c = client.read(); 
				_httpReq += c;

				// last line of client request is blank and ends with \n
				// respond to client only after last line received
				
				if (c == '\n' && currentLineIsBlank) {

					// create page
					client.println("HTTP/1.1 200 OK");
					client.println("Content-Type: application/json");
					client.println();
			
					// create json object
					ProcessRequests(client);

					_httpReq = ""; // finish with request, empty string
					break;
				}
				// every line of text received from the client ends with \r\n
				if (c == '\n') {
					// last character on line of received text
					// starting new line with next character read
					currentLineIsBlank = true;
				}
				else if (c != '\r') {
					// a text character was received from client
					currentLineIsBlank = false;
				}
			} // end if (client.available())
		} // end while (client.connected())
		delay(1);      // give the web browser time to receive the data
		client.stop(); // close the connection
	} // end if (client)
}

// switch LED
void ProcessRequests(EthernetClient cl)
{
	Serial.println(_httpReq);

	//Checks for the URL string 1 or 0
	if (_httpReq.indexOf("led=1") > -1)
	{
		_ledStatus = 1;
	}
	else
	{
		_ledStatus = 0;
	}

	if (_ledStatus)
	{
		Serial.println("ON");
		digitalWrite(_ledPin, HIGH);
		cl.print("{ \"LedState\": true, \"CreatedAt\": null }");
	}
	else
	{
		Serial.println("OFF");
		digitalWrite(_ledPin, LOW);
		cl.print("{ \"LedState\": false, \"CreatedAt\": null }");
	}
}