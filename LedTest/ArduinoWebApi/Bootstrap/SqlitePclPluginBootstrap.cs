using MvvmCross.Platform.Plugins;

namespace ArduinoWebApi.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Sqlite.PluginLoader>
		{}
}
