﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Arduino.Common;
using Newtonsoft.Json;

namespace ArduinoWebApi.Controllers
{
    public class ArduinoController : ApiController
    {
        [Route("api/led/state/{state}")]
        [HttpGet]
        public async Task<IHttpActionResult> SetLedState(bool state)
        {

            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get, "http://192.168.0.20/led=0");
                if (state)
                {
                    request = new HttpRequestMessage(HttpMethod.Get, "http://192.168.0.20/led=1");
                }

                try
                {
                    var result = await client.SendAsync(request);
                    if (!result.IsSuccessStatusCode) return BadRequest("failed");

                    var response = await result.Content.ReadAsStringAsync();

                    var ledResponse = JsonConvert.DeserializeObject<LedResponse>(response);
                    ledResponse.CreatedAt = DateTime.UtcNow;

                    return Ok(ledResponse);
                }
                catch (Exception exception)
                {
                    return BadRequest(exception.ToString());
                }
            }
        }
    }
}
