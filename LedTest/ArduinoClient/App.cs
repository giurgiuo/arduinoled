

using ArduinoClient.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.IoC;
using XLabs.Platform.Services.IO;

namespace ArduinoClient
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .Except(typeof(EventService))
                .AsInterfaces()
                .RegisterAsLazySingleton();
				
            RegisterAppStart<ViewModels.FirstViewModel>();
        }
    }
}