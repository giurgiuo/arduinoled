using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Arduino.Common;
using ArduinoClient.Services;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;

namespace ArduinoClient.ViewModels
{
    public class FirstViewModel 
		: BaseViewModel
    {
        private readonly IEventService _eventService;
        private readonly ILogger _logger;
        private readonly IMvxJsonConverter _jsonConverter;

        // TODO: add user dialogs that works with MVVMCross 4.0

        public FirstViewModel( ILogger logger, IMvxJsonConverter jsonConverter)
        {
            _eventService = Mvx.Resolve<IEventService>();
            _logger = logger;
            _jsonConverter = jsonConverter;
        }

        #region PROPERTY: LedSwitch

        private bool _ledSwitch;

        public bool LedSwitch
        {
            get { return _ledSwitch; }
            set
            {
                if (_ledSwitch == value) return;

                try
                {
                    _logger.SaveLog(MvxTraceLevel.Diagnostic, "Info", "Try to turn the switch to " + value);

                    SwitchAsync();

                    if (_ledSwitch != value)
                    {

                        SwitchFailAsync();
                    }
                    
                    RaisePropertyChanged(() => LedSwitch);
                }
                catch (Exception exception)
                {
                    _logger.SaveLog(MvxTraceLevel.Error, "Error", exception.ToString());

                    // TODO: _userDialogs.Alert("The operation did not succed", "Alert");

                    _eventService.SaveLog(exception.ToString(), exception.Message);
                }
            }
        }

        public async void SwitchFailAsync()
        {
            var errorMessage = string.Format("The switch did not change the state of the LED, state is {0}", _ledSwitch);
            _logger.SaveLog(MvxTraceLevel.Error, "Error", errorMessage);

            // TODO: await _userDialogs.AlertAsync("The switch did not change the state of the LED", "Led state is ");
            
            _eventService.SaveLog("The switch did not change the state of the LED", _ledSwitch.ToString());
        }

        public async void SwitchAsync()
        {
            try
            {
                var isLedSwitchedOn = await SwitchUnsafe();

                _ledSwitch = isLedSwitchedOn;

                _eventService.SaveState(isLedSwitchedOn);
            }
            catch (Exception exception)
            {
                _eventService.SaveLog(exception.ToString(), exception.Message);
            }
        }

        public async Task<bool> SwitchUnsafe()
        {
            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get, "http://192.168.0.7/ArduinoWebApi/api/led/state/false");
                if (_ledSwitch)
                {
                    request = new HttpRequestMessage(HttpMethod.Get, "http://192.168.0.7/ArduinoWebApi/api/led/state/true");
                }

                var result = await client.SendAsync(request);

                if (result.StatusCode != HttpStatusCode.OK) return false;

                var response = await result.Content.ReadAsStringAsync();
                var ledResponse = _jsonConverter.DeserializeObject<LedResponse>(response);

                if (ledResponse.LedState) return true;
            }

            return false;
        }

        #endregion
    }
}
