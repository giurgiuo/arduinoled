﻿using System.Collections.Generic;

namespace ArduinoClient.Repository
{
    public interface IRepository<T> where T : class 
    {
        IEnumerable<T> GetData();
        void Insert(T entry);
        void Update(T entry);
        void Delete(T entry);
    }
}
