﻿namespace ArduinoClient.Repository
{
    public interface IRepositoryFactory
    {
        IRepository<T> Create<T>() where T : class, new();
    }
}
