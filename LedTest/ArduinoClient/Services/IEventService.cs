﻿namespace ArduinoClient.Services
{
    public interface IEventService
    {
        void SaveLog(string exception, string message);

        void SaveState(bool state);
    }
}
