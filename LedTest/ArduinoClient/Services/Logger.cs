﻿using System;
using System.Diagnostics;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.File;

namespace ArduinoClient.Services
{
    public class Logger : ILogger
    {
        private readonly IMvxFileStore _file;
        private string LogPath { get; }

        public Logger(string logPath, IMvxFileStore file)
        {
            _file = file;
            LogPath = logPath;
        }

        public void SaveLog(MvxTraceLevel level, string tag, string message)
        {
            Trace(level, tag, message);

            var text = tag + ":" + level + ":" + message;
            _file.WriteFile(LogPath, text);
        }

        public void Trace(MvxTraceLevel level, string tag, Func<string> message)
        {
            Debug.WriteLine(tag + ":" + level + ":" + message());
        }

        public void Trace(MvxTraceLevel level, string tag, string message)
        {
            Debug.WriteLine(tag + ":" + level + ":" + message);
        }

        public void Trace(MvxTraceLevel level, string tag, string message, params object[] args)
        {
            try
            {
                Debug.WriteLine(tag + ":" + level + ":" + message, args);
            }
            catch (FormatException)
            {
                Trace(MvxTraceLevel.Error, tag, "Exception during trace of {0} {1} {2}", level, message);
            }
        }
    }
}
