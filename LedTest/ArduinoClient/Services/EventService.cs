﻿using System;
using ArduinoClient.Models;
using ArduinoClient.Repository;

namespace ArduinoClient.Services
{
    public class EventService : IEventService
    {
        private readonly IRepositoryFactory _repositoryFactory;
        private readonly IRepository<Log> _logRepository;
        private readonly IRepository<Audit> _auditRepository; 

        public EventService(IRepositoryFactory repositoryFactory)
        {
            _repositoryFactory = repositoryFactory;

            _logRepository = _repositoryFactory.Create<Log>();
            _auditRepository = _repositoryFactory.Create<Audit>();
        }

        public void SaveLog(string exception, string message)
        {
            var log = new Log
            {
                Exception = exception,
                Message = message,
                CreatedAt = DateTime.Now
            };

            _logRepository.Insert(log);
        }

        public void SaveState(bool state)
        {
            var audit = new Audit
            {
                State = state,
                CreatedAt = DateTime.Now
            };

            _auditRepository.Insert(audit);
        }
    }
}
