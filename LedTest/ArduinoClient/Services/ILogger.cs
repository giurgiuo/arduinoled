﻿using MvvmCross.Platform.Platform;

namespace ArduinoClient.Services
{
    public interface ILogger : IMvxTrace
    {
        void SaveLog(MvxTraceLevel level, string tag, string message);
    }
}
