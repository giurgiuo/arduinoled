﻿using System;
using SQLite.Net.Attributes;

namespace ArduinoClient.Models
{
    public class Audit
    {
        [NotNull]
        [PrimaryKey]
        [AutoIncrement]
        public int AuditId { get; set; }

        [NotNull]
        public bool State { get; set; }

        [NotNull]
        public DateTime CreatedAt { get; set; }
    }
}
