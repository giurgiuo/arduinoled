﻿using System;
using SQLite.Net.Attributes;

namespace ArduinoClient.Models
{
    public class Log
    {
        [NotNull]
        [PrimaryKey]
        [AutoIncrement]
        public int LogId { get; set; }

        [NotNull]
        public string Exception { get; set; }

        [NotNull]
        public string Message { get; set; }

        [NotNull]
        public DateTime CreatedAt { get; set; }
    }
}
