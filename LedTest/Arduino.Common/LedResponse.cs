﻿using System;
using SQLite.Net.Attributes;

namespace Arduino.Common
{
    public class LedResponse
    {
        [NotNull]
        [PrimaryKey]
        [AutoIncrement]
        private int LedResponseID { get; set; }

        [NotNull]
        public bool LedState { get; set; }

        public DateTime? CreatedAt { get; set; }
    }
}
