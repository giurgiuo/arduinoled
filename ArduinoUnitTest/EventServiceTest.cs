﻿using System;
using ArduinoClient.Models;
using ArduinoClient.Repository;
using ArduinoClient.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ArduinoUnitTest
{
    [TestClass]
    public class EventServiceTest
    {
        [TestMethod]
        public void IsLogSavedIntoDatabase()
        {
            // Aggregate

            var repositoryFactoryMock = new Mock<IRepositoryFactory>();
            var logRepositoryMock = new Mock<IRepository<Log>>();
            logRepositoryMock.Setup(repository => repository.Insert(It.IsAny<Log>()));
            repositoryFactoryMock.Setup(factory => factory.Create<Log>()).Returns(logRepositoryMock.Object);

            var eventService = new EventService(repositoryFactoryMock.Object);
            
            // Act

            try
            {
                eventService.SaveLog("exception", "message");
            }
            catch (Exception exception)
            {
                //Assert

                Assert.Fail("The service trown the exception: {0}", exception);
            }
        }

        [TestMethod]
        public void IsLedStateSavedIntoDatabase()
        {
            // Aggregate

            var repositoryFactoryMock = new Mock<IRepositoryFactory>();
            var logRepositoryMock = new Mock<IRepository<Audit>>();
            logRepositoryMock.Setup(repository => repository.Insert(It.IsAny<Audit>()));
            repositoryFactoryMock.Setup(factory => factory.Create<Audit>()).Returns(logRepositoryMock.Object);

            var eventService = new EventService(repositoryFactoryMock.Object);

            // Act

            try
            {
                eventService.SaveState(true);
            }
            catch (Exception exception)
            {
                //Assert

                Assert.Fail("The service trown the exception: {0}", exception);
            }
        }
    }
}
